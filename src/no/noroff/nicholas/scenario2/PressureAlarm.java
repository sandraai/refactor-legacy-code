package no.noroff.nicholas.scenario2;

public class PressureAlarm implements Alarm {
    private final double LowPressureThreshold; // 17;
    private final double HighPressureThreshold; // 21;

    boolean alarmOn = false;

    public PressureAlarm (double LowPressureThreshold, double HighPressureThreshold) {      // takes thresholds as args in constructor instead of final values
        this.LowPressureThreshold = LowPressureThreshold;
        this.HighPressureThreshold = HighPressureThreshold;
    }

    public void check(Sensor sensor){                                                       // any sensor object is passed to method instead of creating it in the class
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
