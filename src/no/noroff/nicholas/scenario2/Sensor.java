package no.noroff.nicholas.scenario2;
// Interface for all sensors (I would change method name to be more generic like "getSensorValue()" and in PressureSensor class implement it to mean pressure psi value, but interpreted instructions as that this is a public API that should not be changed)
public interface Sensor {
    double popNextPressurePsiValue();
}
