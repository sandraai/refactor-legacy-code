package no.noroff.nicholas.scenario2;
// Interface for all kinds of alarms
public interface Alarm {
    void check(Sensor sensor);
    boolean isAlarmOn();
}
