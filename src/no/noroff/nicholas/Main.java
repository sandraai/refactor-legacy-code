package no.noroff.nicholas;

import no.noroff.nicholas.scenario2.PressureAlarm;
import no.noroff.nicholas.scenario2.PressureSensor;

public class Main {

    public static void main(String[] args) {
        PressureSensor tireSensor = new PressureSensor();
        PressureAlarm tireAlarm = new PressureAlarm(17, 21);

        tireAlarm.check(tireSensor);
        System.out.println(tireAlarm.isAlarmOn());
    }
}
