package no.noroff.nicholas.scenario1;

import java.util.Random;

public class Sensor {                           // Can use Interface to make sure any sensor implements popNextPressurePsiValue(), which will be called by any alarm connected to the sensor - Open/Closed principle
    public static final double OFFSET = 16;     // so that if we create and use a new sensor the alarm can use it without any changes having to be made to alarm

    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;
        pressureTelemetryValue = samplePressure();
        return OFFSET + pressureTelemetryValue;
    }

    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return pressureTelemetryValue;
    }
}
