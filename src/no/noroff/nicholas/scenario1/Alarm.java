package no.noroff.nicholas.scenario1;

public class Alarm {                                        // Can use Interface to make sure any kind of alarm implements check() and isAlarmOn(), so that we easily can use some other alarm - Open/Closed principle & Interface segregation (one for Alarm, one for Sensor)
    private final double LowPressureThreshold = 17;         // in making Alarm class more generic, creat abstractions for threshold values and pass them when instantiating - Dependency inversion
    private final double HighPressureThreshold = 21;

    Sensor sensor = new Sensor();                           // might be better to be able to pass a Sensor object of any kind to Alarm e.g. when calling check, to make it more loosely coupled - Dependency inversion
    boolean alarmOn = false;

    public void check(){
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
